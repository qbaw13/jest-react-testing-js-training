import { useCallback, useEffect, useState } from 'react';
import { collectInvestmentDetails } from '../../services/investment';

function useInvestmentDetails(amount, numberOfYears) {
  const [investmentDetails, setInvestmentDetails] = useState({});

  const updateInvestmentDetails = useCallback(async () => {
    const investmentDetails = await collectInvestmentDetails({
      amount,
      numberOfYears,
    });
    setInvestmentDetails(investmentDetails);
  }, [amount, numberOfYears]);

  useEffect(() => {
    updateInvestmentDetails();
  }, [updateInvestmentDetails]);

  return investmentDetails;
}

export default useInvestmentDetails;
