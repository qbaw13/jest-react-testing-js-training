import fetchGrowthRate from '../api/fetch-growth-rate';
import calculateAnnualGrowth from '../utils/calculate-annual-growth';

const TOKEN = 'TOKEN';

async function collectAnnualGrowth(amount, numberOfYears) {
  if (
    amount == null ||
    amount <= 0 ||
    numberOfYears == null ||
    numberOfYears <= 0 ||
    numberOfYears > 50
  ) {
    return;
  }
  const growthRate = await fetchGrowthRate(TOKEN);
  return calculateAnnualGrowth(amount, numberOfYears, growthRate);
}

export async function collectInvestmentDetails({ amount, numberOfYears }) {
  const annualGrowth = await collectAnnualGrowth(amount, numberOfYears);

  return {
    annualGrowth,
  };
}
