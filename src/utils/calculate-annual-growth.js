function validate(amount, numberOfYears, growthRate) {
  if (numberOfYears <= 0) {
    throw new Error(
      `Number of years should be positive number, numberOfYears=${numberOfYears}`
    );
  }
  if (amount <= 0) {
    throw new Error(`Amount should be positive number, amount=${amount}`);
  }
  if (growthRate <= 0) {
    throw new Error(
      `Growth rate should be positive number, growthRate=${growthRate}`
    );
  }
}

function calculateAnnualGrowth(amount, numberOfYears, growthRate) {
  validate(amount, numberOfYears, growthRate);
  const annualGrowth = [amount];

  for (let i = 1; i <= numberOfYears; i++) {
    const amountAfterGrowth = annualGrowth[i - 1] * (1 + growthRate);
    annualGrowth.push(amountAfterGrowth);
  }

  return annualGrowth;
}

export default calculateAnnualGrowth;
