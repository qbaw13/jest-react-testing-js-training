import { useState } from 'react';
import { List, ListItem, ListItemText, TextField } from '@material-ui/core';
import useInvestmentDetails from './hooks/useInvestmentDetails';
import './styles.css';

function App() {
  const [amount, setAmount] = useState(null);
  const [numberOfYears, setNumberOfYears] = useState(null);
  const { annualGrowth } = useInvestmentDetails(amount, numberOfYears);

  const handleAmountChange = (event) => {
    if (event == null || event.target == null) {
      return;
    }
    if (event.target.value === '') {
      setAmount(null);
    } else {
      setAmount(Number(event.target.value));
    }
  };

  const handleNumberOfYearsChange = (event) => {
    if (event == null || event.target == null) {
      return;
    }
    if (event.target.value === '') {
      setNumberOfYears(null);
    } else {
      setNumberOfYears(Number(event.target.value));
    }
  };

  const numberOfYearsError =
    numberOfYears != null && (numberOfYears < 0 || numberOfYears > 50);

  return (
    <div className="App">
      <header>
        <h1>Investment panel</h1>
      </header>
      <section>
        <h2>Input</h2>
        <TextField
          id="Amount"
          label="Amount"
          type="number"
          variant="outlined"
          margin="normal"
          onChange={handleAmountChange}
          fullWidth
        />
        <TextField
          error={numberOfYearsError}
          id="Number of years"
          label="Number of years"
          type="number"
          variant="outlined"
          margin="normal"
          onChange={handleNumberOfYearsChange}
          helperText="Input value between 1 and 50"
          fullWidth
        />
      </section>
      <h2>Investment</h2>
      <section>
        {annualGrowth ? (
          <List>
            {annualGrowth.map((amount, index) => (
              <ListItem key={index}>
                <ListItemText>year {index}</ListItemText>
                <ListItemText>{amount}</ListItemText>
              </ListItem>
            ))}
          </List>
        ) : (
          <span>Please fill in investment form</span>
        )}
      </section>
    </div>
  );
}

export default App;
