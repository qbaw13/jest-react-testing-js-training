import { collectInvestmentDetails } from './investment';
import mockedFetchGrowthRate from '../api/fetch-growth-rate';

jest.mock('../api/fetch-growth-rate');

describe('investment', () => {
  describe('calculate investment details', () => {
    afterEach(() => {
      mockedFetchGrowthRate.mockClear();
    });

    it('should return calculated annual growth', async () => {
      mockedFetchGrowthRate.mockResolvedValue(0.0645);

      const actual = await collectInvestmentDetails({
        amount: 345,
        numberOfYears: 4,
      });

      expect(actual).toMatchObject({
        annualGrowth: [
          345, 367.2525, 390.94028625, 416.15593471312496, 442.9979925021215,
        ],
      });
    });

    it('should call annual growth api', async () => {
      await collectInvestmentDetails({ amount: 345, numberOfYears: 4 });

      expect(mockedFetchGrowthRate).toHaveBeenCalledWith('TOKEN');
    });
  });
});
